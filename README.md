# mailbox.sexy

## How to install

* Have docker and ansible installed and docker running
* Create a password

```bash
ansible-vault create vault.yml
# Inside the file
---
password: Your password
```

* Run the playbook

```bash
sudo ansible-playbook mailbox.sexy.yml
```

## How it works

* Tor runs as DNS server and transparent proxy for .onion hosts

  The `hostname` file is shared to postfix so it can know which one is
  the local address.

* Unbounds runs as DNS resolver, sending queries for .onion to the Tor
  container, which will reply with an IP address in a private subnet.

* Postfix runs as email server.  It will receive email from the user to
  other mailboxes and from other mailboxes to the user.

  It authenticates the user with Dovecot via a shared Unix socket.

  It needs access to Tor's `hostname` file to get its own domain.

  It needs access to the SSL certificates to serve allow for encrypted
  connections with the client (and optional encrypted connections with
  remote mailboxes, even though the transport layer is already secured
  by Tor).

  It creates the user.  No password needed at this stage since it
  authenticates with Dovecot.

  It shares the email storage with Dovecot.

* Dovecot authenticates the user and serves email via IMAP and POP3.

  It needs access to SSL certificates for encrypted connections with the
  client.

  It creates the user with a password.

  It shares the email storage with Postfix.

* Darkhttpd just serves the autoconfig file so the process is faster
  using Thunderbird.  Other MUAs yet to come.

* GNUTLS creates the certificates that are going to be used by Dovecot,
  Postfix and the localhost.
