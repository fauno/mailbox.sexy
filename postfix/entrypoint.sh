#!/bin/sh

# Crear todos los archivos que pide postfix
install -d /usr/share/doc/postfix/readme
while ! postfix set-permissions >/tmp/p 2>&1 ; do
  cat /tmp/p | cut -d : -f 2 | xargs dirname | xargs install -d
  cat /tmp/p | cut -d : -f 2 | xargs touch
done

rm /tmp/p
exec postfix start
